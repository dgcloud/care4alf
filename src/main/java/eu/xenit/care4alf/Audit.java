package eu.xenit.care4alf;

import com.github.dynamicextensionsalfresco.webscripts.annotations.WebScript;
import org.springframework.stereotype.Component;

/**
 * Created by willem on 9/16/15.
 */
@Component
@WebScript(baseUri = "/xenit/care4alf/audit", families = {"care4alf"}, description = "Audit")
public class Audit {

}
